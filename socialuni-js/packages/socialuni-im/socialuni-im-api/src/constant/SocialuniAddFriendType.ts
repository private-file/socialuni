import SocialuniConstStatus from "@socialuni/socialuni-constant/constant/status/SocialuniConstStatus"
import SocialuniCommonStatus from "@socialuni/socialuni-constant/constant/status/SocialuniCommonStatus"

export default class SocialuniAddFriendType {
  static readonly apply: string = SocialuniConstStatus.apply
  static readonly accept: string = SocialuniConstStatus.accept
}
