import SocialuniConstStatus from "@socialuni/socialuni-constant/constant/status/SocialuniConstStatus";
import SocialuniCommonStatus from "@socialuni/socialuni-constant/constant/status/SocialuniCommonStatus";

export default class SocialuniAddFriendStatus extends SocialuniCommonStatus {
    static readonly init: string =  SocialuniConstStatus.init
}
