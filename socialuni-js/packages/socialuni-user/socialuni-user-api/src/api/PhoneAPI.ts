import socialuniUserRequest from "../request/socialuniUserRequest";
import UniProviderLoginQO from "@socialuni/socialuni-api-base/src/model/login/UniProviderLoginQO";
import CenterUserDetailRO from "@socialuni/socialuni-api-base/src/model/social/CenterUserDetailRO";
import SocialSendAuthCodeQO from "@socialuni/socialuni-api-base/src/model/phone/SocialSendAuthCodeQO";
import SocialPhoneNumLoginQO from "@socialuni/socialuni-api-base/src/model/phone/SocialPhoneNumLoginQO";
import SocialuniMineUserRO from "@socialuni/socialuni-api-base/src/model/user/SocialuniMineUserRO";
import BindWxPhoneNumQO from "@socialuni/socialuni-api-base/src/model/phone/BindWxPhoneNumQO";
import RefreshWxSessionKeyQO from "@socialuni/socialuni-api-base/src/model/phone/RefreshWxSessionKeyQO";


export default class PhoneAPI {
  static bindSocialuniPhoneNum (loginData: UniProviderLoginQO) {
    return socialuniUserRequest.post<CenterUserDetailRO>('socialuni/phone/bindSocialuniPhoneNum', loginData)
  }

  static sendAuthCodeAPI (phoneNum: string) {
    return socialuniUserRequest.post('socialuni/phone/sendAuthCode', new SocialSendAuthCodeQO(phoneNum))
  }

  static bindPhoneNumAPI (phoneNum: string, authCode: string) {
    const phoneNumObj: SocialPhoneNumLoginQO = new SocialPhoneNumLoginQO(phoneNum, authCode)
    return socialuniUserRequest.post<SocialuniMineUserRO>('socialuni/phone/bindPhoneNum', phoneNumObj).then(res => {
      return res.data
    })
  }

  //微信绑定手机号使用
  static bindWxPhoneNumAPI (bindWxPhoneNumQO: BindWxPhoneNumQO) {
    return socialuniUserRequest.post<CenterUserDetailRO>('socialuni/phone/bindWxPhoneNum', bindWxPhoneNumQO)
  }

  //微信绑定手机号使用
  static refreshWxSessionKeyAPI (code: string) {
    return socialuniUserRequest.post('socialuni/phone/refreshWxSessionKey', new RefreshWxSessionKeyQO(code))
  }
}
