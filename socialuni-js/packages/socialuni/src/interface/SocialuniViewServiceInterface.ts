import {Vue} from "vue-class-component";

export interface SocialuniViewServiceInterface {
    instance: Vue

    initService(instance: Vue)
}
