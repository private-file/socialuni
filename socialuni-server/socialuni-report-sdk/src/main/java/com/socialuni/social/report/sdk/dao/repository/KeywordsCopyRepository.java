package com.socialuni.social.report.sdk.dao.repository;


import com.socialuni.social.report.sdk.dao.DO.KeywordsCopyDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author qinkaiyuan
 * @date 2018-10-17 21:59
 */
public interface KeywordsCopyRepository extends JpaRepository<KeywordsCopyDO, Integer> {

}


